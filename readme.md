# Kent Outreach - DAR^2

## Authors:

* [Dana Clemmer](https://github.com/dclemmer2).
* [Alisia Llavore](https://github.com/allavore2022).
* [Ryan Riveria](https://github.com/rrivera123).
* [Ryan Hendrickson](https://gitlab.com/rynhndrcksn).

## Summary:

Group based project using Agile methodologies where we competed against other groups. Client wanted a website for his outreach program based in Kent, WA. Client needs were: user able to submit form to request assistance, user able to find pertinent information about the outreach program, user able to view a summary of their request, and admin page to see all requests and "check off" users that had been contacted by outreach program.

## Technologies:
* HTML
* CSS
    * Bootstrap 4
* JavaScript
    * JQuery
* PHP
* MySQL
* IDE: PHPStorm